# Front-End #

### Install dependencies project

front-end requires [Angular 2 + WebPack](https://webpack.github.io/).

Install the dependencies and devDependencies and start the server.

```sh
$ cd frontEnd-test-Joyjet
$ npm install
$ npm start
$ Project configured to open in "http://0.0.0.0:3000/"
$ configure this in server/main.js
```